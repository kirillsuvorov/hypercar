from django.db import models
import json

service_caption = {
    "change_oil": "Change Oil",
    "inflate_tires": "Inflate Tires",
    "diagnostic": "Get diagnostic test"
}
services_queue = {
    "change_oil": [],
    "inflate_tires": [],
    "diagnostic": []
}

with open("data_file.json", "w") as write_file:
    json.dump(services_queue, write_file)
ticket_number = 0
next = 0
wait_time = 0
# Create your models here.
