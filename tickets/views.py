from django.views import View
from django.shortcuts import render
from django.shortcuts import redirect
from tickets.models import service_caption, ticket_number, next, wait_time
import json


class WelcomeView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'tickets/welcome.html')


class MenuView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'tickets/menu.html', context={'services': service_caption})


class ServiceView(View):
    def get(self, request, service_name, *args, **kwargs):
        with open("data_file.json", "r") as read_file:
            services = json.load(read_file)
        if service_name == 'change_oil':
            global wait_time
            wait_time = len(services["change_oil"]) * 2
        elif service_name == "inflate_tires":
            wait_time = len(services["change_oil"]) * 2 + len(services["inflate_tires"]) * 5
        elif service_name == "diagnostic":
            wait_time = len(services["change_oil"]) * 2 + len(services["inflate_tires"]) * 5 + len(services["diagnostic"] * 30)

        global ticket_number
        ticket_number += 1
        services[service_name].append(ticket_number)

        with open("data_file.json", "w") as write_file:
            json.dump(services, write_file)

        return render(request, 'tickets/show_ticket_wait.html', context={
            'service': service_name,
            'ticket_number': ticket_number,
            'minutes_to_wait': wait_time,
        })


class ProcessingView(View):
    def get(self, request, *args, **kwargs):
        with open("data_file.json", "r") as read_file:
            services = json.load(read_file)
        context = {
            'service': services
        }
        return render(request, 'tickets/processing.html', context)

    def post(self, request, *args, **kwargs):
        global next
        with open("data_file.json", "r") as read_file:
            services = json.load(read_file)
        if services["change_oil"]:
            next = services["change_oil"].pop(0)
        elif services["inflate_tires"]:
            next = services["inflate_tires"].pop(0)
        elif services["diagnostic"]:
            next = services["diagnostic"].pop(0)
        else:
            next = 0

        with open("data_file.json", "w") as write_file:
            json.dump(services, write_file)
        #return render(request, 'tickets/next.html', context={"next_num": next})
        #return NextView.get(self, request, next)
        return redirect('/next')


class NextView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'tickets/next.html', context={"next_num": next})


